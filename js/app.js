

var Article = React.createClass({

  propTypes: {

    data: React.PropTypes.shape({

      author: React.PropTypes.string.isRequired,
      text: React.PropTypes.string.isRequired,
      bigText:  React.PropTypes.string.isRequired,

    })

  },
  getInitialState: function() {
    return {
      visible: false,
      rating: 0,
      eshe_odno_svoistvo: 'qweqwe',
    }
  },
  showMoreText: function(e) {
    e.preventDefault();
    // var visibleState = (this.props.visible ? false : true);
    this.setState({
      visible: true,
      rating: 555,
      eshe_odno_svoistvo: 'hello'
    }, function() {
      alert("rating has been changed!");
    })
  },
  render: function() {
    console.log("render Article");

    var articleItem = this.props.data;
    var dynamicItem = this.state;

    var author = articleItem.author;
    var text = articleItem.text;
    var bigText = articleItem.bigText;

    var visible = dynamicItem.visible;

    console.log('render', this);

    return (
      <div className="article">
        <p className="article__author">{author}:</p>

        <a href="#"
          onClick={this.showMoreText}

          className={'article__show-more-text ' +
            (visible ? 'none': '')
          }>
          Tap On Me! </a>
        <p className={
            "article__text " + (visible ? 'none': '')
          }>{text}</p>

        <p className={
            "article__big-text " + (visible ? '': 'none')
          }>{bigText}</p>
      </div>
    );

  }

});

var News = React.createClass({
  propTypes: {
    news_data: React.PropTypes.array.isRequired,
  },
  getInitialState: function() {
    return {
      click: 0,
    }
  },
  render: function() {
    console.log("render News");

    var news = this.props.news_data;
    var newsCounter = news.length;

    var allNews;

    if (newsCounter > 0) {
      allNews = news.map(function(item, index) {
        return (
          <div key={index}>
            <Article data={item} />
          </div>
        )
      });
    } else {
      allNews = <p>Новостей нет! Удачного дня!</p>
    }

    var newsCounterClass = newsCounter > 0 ? '': 'none';

    return (
      <div className="news">
          {allNews}
          <strong className = {
              "news__counter " + newsCounterClass
            }> All News = {newsCounter} </strong>
      </div>
    );
  }
});

var TestInput = React.createClass({

  componentDidMount: function() { // фокус в инпут
    ReactDOM.findDOMNode(this.refs.myTestInput).focus();
  },
  componentWillReceiveProps: function(nextProp) {
    this.setState({
      likesIncreasing: nextProps.likeCount > this.props.likeCount
    });
  },
  inputChanged: function(e){
    e.preventDefault();

    console.log("inputChanged", this);

    this.setState ({
      inputValue: e.target.value
    })
  },
  getInitialState: function() {
    return {
      inputValue: "test..."
    }
  },
  showInputInAlert: function() {
    console.log(this.refs);
    alert(ReactDOM.findDOMNode(
      this.refs.myTestInput).value);
  },
  render: function() {


    var inputValue = this.state.inputValue;

    return (
      <div className='input'>
        <input
          className='input-window'
          defaulValue=''
          placeholder='type something ...'
          ref='myTestInput'/>
        <button onClick={this.showInputInAlert}
          className='input-btn'>
          Tap
        </button>
      </div>
    );
  }
});

var Register = React.createClass({

  getInitialState: function() { // устанавливаем начальное значение state
    return {
      validateName: false,
      validateMail: false,
      validatePassword: false,
      validateRegisterBtn: false
    };
  },

  componentDidMount: function() {
    ReactDOM.findDOMNode(this.refs["name"]).focus();
  },

  onChangeName: function() {
    var name = ReactDOM.findDOMNode(this.refs["name"]).value.trim();

    if (name.length > 0) {
      this.setState({validateName: true}, function functionName() {
        this.checkInputData();
      });
    } else {
      this.setState({validateName: false}, function functionName() {
        this.checkInputData();
      });
    } 
  },
  onChangeMail: function() {
    var mail = ReactDOM.findDOMNode(this.refs["mail"]).value.trim();

    if (mail.length > 0) {
      this.setState({validateMail: true}, function functionName() {
        this.checkInputData();
      });
    } else {
      this.setState({validateMail: false}, function functionName() {
        this.checkInputData();
      });
    }

  },
  onChangePassword: function() {
    var password = ReactDOM.findDOMNode(this.refs["password"]).value.trim();
    var passwordAgain = ReactDOM.findDOMNode(this.refs["passwordAgain"]).value.trim();

    if ((password.length > 0) && (password == passwordAgain)) {
      this.setState({validatePassword: true}, function() {
        this.checkInputData();
      });
    } else {
      this.setState({validatePassword: false}, function() {
        this.checkInputData();
      });
    }
  },

  checkInputData: function(){
    var validateName = this.state.validateName;
    var validateMail = this.state.validateMail;
    var validatePassword = this.state.validatePassword;

    console.log("inputData = " + validateName +","+ validateMail +","+ validatePassword);

    if ((validateName == true) &&
      (validateMail == true) &&
      (validatePassword == true)) {

      this.setState({validateRegisterBtn: true});
      console.log("validateRegisterBtn TRUE!", this);
    } else {
      this.setState({validateRegisterBtn: false});
      console.log("validateRegisterBtn FALSE!", this);
    }
  },

  onClickRegisterBtnHandler: function(e) {
    e.preventDefault();

    var validateBtn = this.state.validateRegisterBtn;

    if (validateBtn == true) {

      var name = ReactDOM.findDOMNode(this.refs["name"]).value.trim();
      var mail = ReactDOM.findDOMNode(this.refs["mail"]).value.trim();

      alert(name + "\n" + mail);

    } else {

      alert("Enter Data, please)");

    }
  },

  render: function() {

    console.log("render Register", this);

    return (
      <form className="reg">
        <h3 className="reg-logo">Register</h3>
        <input
          type='text'
          className="reg-name"
          defaulValue=''
          placeholder='Your name'
          onChange={this.onChangeName}
          ref='name'
        />
        <input
          className='reg-mail'
          defaulValue=''
          placeholder='Your mail'
          onChange={this.onChangeMail}
          ref='mail'
        />
        <input
          className='reg-password'
          defaulValue=''
          placeholder='Your password'
          onChange={this.onChangePassword}
          ref='password'
        />
        <input
          className='reg-password'
          defaulValue=''
          placeholder='Repeat password'
          onChange={this.onChangePassword}
          ref='passwordAgain'
        />

        {/*берем значение disabled атрибута из state*/}

        <button
          className="reg-btn"
          ref='btn'
          onClick={this.onClickRegisterBtnHandler}
          disabled={!this.state.validateRegisterBtn}
           >
          Sing UP
        </button>
      </form>
    );
  }

});

var App = React.createClass({
  render: function() {
    return (
      <div className="app">
        <Register />
      </div>
    );
  }
});

ReactDOM.render(
    <App />,
    document.getElementById('root')
);
